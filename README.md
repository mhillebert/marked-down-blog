# Marked Down Blog
A Symfony bundle which uses markdown for posts. No database necessary.

### Installation
1. Add `mhbert/marked-down-blog: '^0.1'` to the required section of your `composer.json`
```
"require": {
    "mhbert/marked-down-blog": "^0.1"
}
```

1. Add a repository section to your `composer.json` section
```
"repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/mhillebert/marked-down-blog.git"
        }
    ]
```

### Configure
1. Create a `marked_down_blog.yaml` file.
```
# config/packages/marked_down_blog.yaml
marked_down_blog:

    # Class that uses Symfony's Filesystem component
    file_system:          symfony.component.filesystem.filesystem

    # Class that uses Symfony's Finder component
    file_finder:          symfony.component.finder.finder

    # Service reference for caching.
    file_cache:           cache.app

    # Directory which you are putting the markdown to parse.
    content_dir:          '%kernel.project_dir%/posts/'

    # Theme for all non-post pages.
    theme:                '@!MarkedDownBlog/basic'

    # items per page.
    items_per_page:       10

    # Definition of any metadata for the service to use when parsing.
    meta_definition:

        # The delimiter you prefer to begin and end the metadata at the top of each markdown file.
        start_end_delimiter:  '---'

        # The separator character used to separate metadata property from its value.
        property_value_separator: ':'

        # A key-value pair of all the metadata properties you intend to use. If a value is provided, it will be used as a default value for all metadata. Therefore the need to define it within each markdown file is not necessary. Use a `~` character to specify no-value. These are the defaults:
        meta_properties:
            title:                null
            description:          null
            status:               null
            date:                 null
            template:             post
            theme:                '@!MarkedDownBlog/basic'

        # Key-value pairs to replace during the processing of the markdown. E.g. `images: /images` will search the markdown for `%images%` and replace it with `/images`
        replacements:
            assets_url:           /assets/images
```

1. Add routing:
Add the `marked_down_blog.yaml` routing file.
```
# config/routes/marked_down_blog.yaml
_marked_down_blog:
    resource: '@MarkedDownBlogBundle/Resources/config/routes.xml'
```

2. Add twig global variables:
Add global variables for all pages to twig.
```
#config/packages/twig.yaml
twig:
    default_path: '%kernel.project_dir%/templates'
    globals:
        site_name: '<YOUR SITE NAME>'
        site_tagline: "<YOUR SITE TAG LINE>"
        social:
            facebook: ~
            twitter: ~
```

### Custom themes
Create custom themes and use the defined variables in the twig globals, and metadata_definition. Then 
set the proper property in the `config/packages/marked_down_blog.yaml` eg. `theme`, and 
optionally `template`, `theme` in `meta-properties` section.