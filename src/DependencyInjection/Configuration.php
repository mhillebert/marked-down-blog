<?php
namespace MHbert\MarkedDownBlog\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $rootNode = new TreeBuilder('marked_down_blog');

        $rootNode->getRootNode()
        ->children()
            ->scalarNode('file_system')
                ->defaultValue('symfony.component.filesystem.filesystem')
                ->info('Class that uses Symfony\'s Filesystem component')
            ->end()
            ->scalarNode('file_finder')
                ->defaultValue('symfony.component.finder.finder')
                ->info('Class that uses Symfony\'s Finder component')
            ->end()
            ->scalarNode('file_cache')
                ->defaultValue('cache.app')
                ->info('Service reference for caching.')
            ->end()
            ->scalarNode('content_dir')
                ->defaultValue('%kernel.project_dir%/posts/')
                ->info('Directory which you are putting the markdown to parse.')
            ->end()
            ->scalarNode('date_format')
                ->defaultValue('F j, Y')
                ->info('Date/Time format string')
            ->end()
            ->scalarNode('theme')
                ->defaultValue('@!MarkedDownBlog/basic')
                ->info('Theme for all non-post pages.')
            ->end()
            ->arrayNode('social')
                ->addDefaultsIfNotSet()
                ->ignoreExtraKeys(false)
                ->info("Social media links. Service is the key, href is the value. eg. ` facebook: https://facebook.com/example`")
            ->end()
            ->integerNode('items_per_page')
                ->defaultValue(10)
                ->info('items per page.')
            ->end()
            ->arrayNode('meta_definition')
                ->addDefaultsIfNotSet()
                ->info("Definition of any metadata for the service to use when parsing.")
                ->children()
                    ->scalarNode('start_end_delimiter')
                        ->defaultValue('---')
                        ->info('The delimiter you prefer to begin and end the metadata at the top of each markdown file.')
                    ->end()
                    ->scalarNode('property_value_separator')
                        ->defaultValue(':')
                        ->info('The separator character used to separate metadata property from its value.')
                    ->end()
                    ->arrayNode('meta_properties')
                        ->ignoreExtraKeys(false)
                        ->addDefaultsIfNotSet()
                        ->info('A key-value pair of all the metadata properties you intend to use. If a value is provided, it will be used as a default value for all metadata. Therefore the need to define it within each markdown file is not necessary. Use a `~` character to specify no-value. These are the defaults:')
                        ->children()
                            ->scalarNode('title')->defaultNull()->end()
                            ->scalarNode('description')->defaultNull()->end()
                            ->scalarNode('status')->defaultNull()->end()
                            ->scalarNode('date')->defaultNull()->end()
                            ->scalarNode('template')->defaultValue('post.html.twig')->end()
                            ->scalarNode('theme')->defaultValue('@!MarkedDownBlog/basic')->end()
                        ->end()
                    ->end()
                    ->arrayNode('replacements')
                        ->ignoreExtraKeys(false)
                        ->addDefaultsIfNotSet()
                        ->info('Key-value pairs to replace during the processing of the markdown. E.g. `images: /images` will search the markdown for `%images%` and replace it with `/images`')
                        ->children()
                            ->scalarNode('assets_url')->defaultValue('/assets/images')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ->end();

        return $rootNode;
    }

}