<?php
namespace MHbert\MarkedDownBlog\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;


class MarkedDownBlogExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $configuration = $this->getConfiguration($configs, $container) ?? [] ;
        $config = $this->processConfiguration($configuration, $configs);

        $definition = $container->getDefinition('mhbert_marked_down_blog.content_parser');
        $definition->setArgument(0, $config['content_dir']);
        $definition->setArgument(1, $config['meta_definition']);
        $definition->setArgument(2, new Reference($config['file_system']));
        $definition->setArgument(3, new Reference($config['file_finder']));

        $definition = $container->getDefinition('mhbert_marked_down_blog.controller');
        $definition->setArgument(0, $container->getParameter('kernel.environment'));
        $definition->setArgument(1, $config['theme']);
        $definition->setArgument(2, $config['social']);
        $definition->setArgument(3, $config['date_format']);
        $definition->setArgument(4, new Reference($config['file_cache']));
        $definition->setArgument(5, new Reference('mhbert_marked_down_blog.content_parser'));
        $definition->setArgument(6, new Reference('Symfony\Component\Routing\Generator\UrlGeneratorInterface'));
        $definition->setArgument(7, $config['items_per_page']);
    }

    public function getAlias()
    {
        return "marked_down_blog";
    }


}