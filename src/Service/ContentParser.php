<?php
namespace MHbert\MarkedDownBlog\Service;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class ContentParser
{
    private array $metadataDefinition;
    private Filesystem $filesystem;
    private Finder $finder;
    private UrlGeneratorInterface $router;
    private $webRoot;

    public function __construct(string $contentDir, array $metadataDefinition, Filesystem $filesystem, Finder $finder, UrlGeneratorInterface $router )
    {
        $this->contentDir = $contentDir;
        $this->metadataDefinition = $metadataDefinition;
        $this->filesystem = $filesystem;
        $this->finder = $finder;
        $this->router = $router;
        $this->webRoot = $this->router->generate('marked_down_blog_home');
        foreach ($metadataDefinition['replacements'] as $key => $value)
        {
            unset($this->metadataDefinition['replacements'][$key]);
            $this->metadataDefinition['replacements']['%'.$key.'%'] = $value;
        }

    }

    public function extractAllPosts(int $itemsPerPage):\stdClass
    {
        if (!$this->filesystem->exists($this->contentDir)) {
            $this->filesystem->mkdir($this->contentDir);
        }
        $posts = $pagedPosts = [];
        $files = $this->finder->in($this->contentDir)->name('*.md')->files()->getIterator();
        foreach ($files as $index => $file) {
            try {
                $metadata = $this->extractDataFromFile($file->getPathname(), true);
            }Catch(FileNotFoundException $e) {
                continue;
            }
            if (property_exists($metadata, 'date')) {
                $date = $metadata->date;
            } else {
                $date = (new \DateTimeImmutable())->setTimeStamp($file->getMTime());
            }
            $markdown = $metadata->markdown;
            unset($metadata->markdown);
            $posts[] = [
                'metadata' => $metadata,
                'content' => $markdown,
                'date' => $date
            ];
        }
        if ($posts) {
            usort($posts,[$this, 'dateSort']);
            $pagedPosts = array_chunk($posts, $itemsPerPage);
        }

        $info = new \stdClass();
        $info->pagedPosts = $pagedPosts;
        $info->totalPages = count($pagedPosts);
        $info->totalPosts = count($posts);
        return $info;
    }

    public function extractDataFromFile(string $file, bool $onlyMeta = false): \stdClass
    {
        $file = str_replace([$this->contentDir, '.md'], '', $file);
        $pathname = $this->contentDir . $file . '.md';
        if (!$this->filesystem->exists($pathname)) {
            throw new FileNotFoundException();
        }
        $post = new \stdClass();
        $post->url = $this->webRoot . '/' . str_replace([$this->contentDir, '.md'], '', $file);
        foreach ($this->metadataDefinition['meta_properties'] as $property => $value) {
            $post->{$property} = $value;
        }
        $markdown = '';
        $isMetadataLine = false;
        $metaDelimiterRegex = $this->metadataDefinition['start_end_delimiter'];
        $iterator = $this->read($pathname);
        foreach ($iterator as $lineNum => $line) {
            if ($line === $metaDelimiterRegex) {
                $isMetadataLine = $lineNum === 0;
                continue;
            }
            if ($isMetadataLine) {
                $data = $this->processMetadataLine($line);
                $post->{$data['property']} = $data['value'];
                continue;
            }

            if ($onlyMeta) {
                break;
            }

            $markdown .= $line . PHP_EOL;
        }
        $now = new \DateTimeImmutable();

        $post->markdown = $onlyMeta ? null : $this->processMarkdown($markdown);

        if (property_exists($post, 'date') && $post->date > $now) {
            throw new FileNotFoundException();
        }

        return $post;
    }

    private function processMarkdown(string $markdown)
    {
        return str_replace(array_keys($this->metadataDefinition['replacements']), $this->metadataDefinition['replacements'], $markdown);
    }

    private function processMetadataLine(string $line)
    {
        $parts = explode($this->metadataDefinition['property_value_separator'], $line, 2);
        if (count($parts) != 2) {
            throw new \Exception("Error parsing metadata. Expecting 2 parts separated by `{$this->metadataDefinition['property_value_separator']}` in `{$line}`.");
        }
        list($property, $value) = $parts;

        $property = strtolower(trim($property));
        $value = trim($value);

        if ($property === 'template' && strpos($value, '.') === false) {
            $value .= '.html.twig';
        }

        if (preg_match("(date|time)", $property) === 1) {
            $value = new \DateTimeImmutable($value);
        }

        return [
            'property' => $property,
            'value' => $value
        ];
    }

    private function read($path)
    {
        $handle = fopen($path, "rb");

        while(!feof($handle)) {
            yield trim(fgets($handle));
        }

        fclose($handle);
    }

    private function dateSort($a, $b) {
        return $a['date'] < $b['date'];
    }
}