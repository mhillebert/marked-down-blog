<?php
namespace MHbert\MarkedDownBlog\Controller;

use MHbert\MarkedDownBlog\Service\ContentParser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MarkedDownBlogController extends AbstractController
{
    private string $itemsPerPage;
    private CacheInterface $cache;
    private ContentParser $contentParser;
    private string $environment;

    private string $theme;
    private array $social;
    private string $dateFormat;
    private UrlGeneratorInterface $router;
    private string $webRoot;

    public function __construct(string $environment, string $theme, array $social, string $dateFormat, CacheInterface $cache, ContentParser $contentParser, UrlGeneratorInterface  $router, int $itemsPerPage = 10)
    {
        $this->environment = $environment;
        $this->cache = $cache;
        $this->contentParser = $contentParser;
        $this->itemsPerPage = $itemsPerPage;
        $this->theme = $theme;
        $this->dateFormat = $dateFormat;
        $this->social = $social;
        $this->router = $router;
        $this->webRoot = $router->generate('marked_down_blog_home');
    }

    public function home()
    {
        $pageCount = $this->itemsPerPage;
        $postService = $this->contentParser;
        try {
            if ($this->environment === 'dev') {
                $info=$postService->extractAllPosts($pageCount);
            } else {
                $info = $this->cache->get('pagedPosts', function() use ($pageCount, $postService) {
                    return $postService->extractAllPosts($pageCount);
                });
            }
        } catch (\RuntimeException $e) {
            throw new NotFoundHttpException("Something went wrong.");
        }

        $template = $this->theme . '/' . 'home.html.twig';
        return $this->cacheAndRender($template, [
            'posts' => $info->pagedPosts[0] ?? [],
            'total_pages' => $info->totalPages,
            'total_posts' => $info->totalPosts,
            'current_page' => 1,
            'next_page' => $info->totalPages > 1 ? 2 : null,
            'previous_page' => null,
            'social' => $this->social,
            'web_root' => $this->webRoot,
            'date_format' => $this->dateFormat
        ]);
    }

    public function page(int $page)
    {
        $pageCount = $this->itemsPerPage;
        $postService = $this->contentParser;
        try {
            if ($this->environment === 'dev') {
                $info=$postService->extractAllPosts($pageCount);
            } else {
                $info = $this->cache->get('pagedPosts', function() use ($pageCount, $postService) {
                    return $postService->extractAllPosts($pageCount);
                });
            }

            $pageIndex = $page < 1 ? 1 : $page - 1;
            $posts = $info->pagedPosts[$pageIndex];
            $template = $this->theme . '/' . 'home.html.twig';

        } catch (\RuntimeException $e) {
            throw new NotFoundHttpException("Something went wrong.");
        }

        return $this->cacheAndRender($template, [
            'posts' => $posts,
            'current_page' => $page,
            'total_pages' => $info->totalPages,
            'total_posts' => $info->totalPosts,
            'next_page' => $page < $info->totalPages ? $page + 1 : null,
            'previous_page' => $page - 1 < 1 ? null : $page - 1,
            'social' => $this->social,
            'web_root' => $this->webRoot,
            'date_format' => $this->dateFormat
        ]);
    }

    public function post(string $slug)
    {
        $postService = $this->contentParser;

        try {
            if ($this->environment === 'dev' ) {
                $post = $postService->extractDataFromFile($slug);
            } else {
                $post = $this->cache->get(crc32($slug), function() use ($slug, $postService) {
                    return $postService->extractDataFromFile($slug);
                });
            }
        } catch (FileNotFoundException $e) {
            throw new NotFoundHttpException("Page not found.");
        }

        $template = $this->theme .'/' . $post->template;
        return $this->cacheAndRender($template, [
            'slug' => $slug,
            'post' => $post,
            'web_root' => $this->webRoot,
            'date_format' => $this->dateFormat
        ]);
    }

    private function cacheAndRender(string $template, array $pageVars = [], int $cacheTime = 3600)
    {
        // somehow create a Response object, like by rendering a template
        $response = $this->render($template, $pageVars);

        // cache publicly for 3600 seconds
        $response->setPublic();
        $response->setMaxAge($cacheTime);

        // (optional) set a custom Cache-Control directive
        $response->headers->addCacheControlDirective('must-revalidate', true);

        return $response;
    }
}