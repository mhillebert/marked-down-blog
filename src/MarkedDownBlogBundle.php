<?php
namespace MHbert\MarkedDownBlog;

use MHbert\MarkedDownBlog\DependencyInjection\MarkedDownBlogExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MarkedDownBlogBundle extends Bundle
{
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new MarkedDownBlogExtension();
        }

        return $this->extension;
    }

}